import toml
import json
import os
import xlwings as xw
import datetime
import calendar
import platform
import time

conf:dict = toml.load("conf.toml")
setting:dict = toml.load("settings.toml")

# 获取配置信息
r_name:str = conf.get("name")
r_tel:str = conf.get("tel")
r_id:str = conf.get("id")
r_month:int = conf.get("month")
r_year:int = conf.get("year")
r_datetime:datetime.datetime = datetime.datetime(year=r_year, month=r_month,day=1)
r_reasons:list[str] = conf.get("reasons")
r_alrady_fill_hours:float = 0
r_key:str = 0
r_cacheenable:bool = setting.get("cache")

print(f"{r_name} 现在正在填写{r_datetime.strftime('%Y-%m')}的报表")
r_totalhours:float = float(input("你想编多久的\n->"))
assert(r_totalhours > 0)


last_row:list = []
filled_list:list = []
cacher:dict = {"c":[],"hour":0}

def hr() -> None:
    '''
    分隔符
    '''
    print("==="*40)

def always_display() -> None:
    '''
    显示信息
    '''
    print(f"{r_name} 现在正在填写{r_datetime.strftime('%Y-%m')}的报表")
    print("预设工作内容")
    for i in range(len(r_reasons)):
        print(f"- {i} {r_reasons[i]}")
    
    print(f"已填写{len(filled_list)}行")
    print(f"已填写{r_alrady_fill_hours}小时，剩余{r_totalhours-r_alrady_fill_hours}小时未填写")
    print("Tips: \
          \n- 在日期选择时使用`wq`以结束填写，使用`dl`移除上一行填写的内容，\
          \n - 使用`p`打印目前已经填写的内容，使用`lc`从缓存中恢复已填写的内容\
          \n - 在选择工作事项时手动键入以创建新事项")

def new_row() -> list:
    '''
    创建新的一行
    '''
    return [r_month,0,"上午",r_name,r_id,r_tel,0,0,0,r_name]

def os_clear() -> None:
    '''
    根据操作系统`清屏`'''
    sysstr = platform.system()
    if sysstr == "Windows":
        os.system("cls")
    else:
        os.system("clear")

def os_msg(msg:str) -> None:
    '''
    清屏并显示提示
    @paramas: msg 要显示的信息
    '''
    os_clear()
    print("MSG"+msg)
    time.sleep(1)
    os_clear()

def print_lastrow() -> None:
    '''
    显示上一行填写的内容
    '''
    if last_row != []:
        hr()
        print("上一行填写内容\n[+] ->"+str(last_row))
        hr()

def remove_last() -> None:
    '''
    删除最近填写的一行
    '''
    os_clear()
    global r_alrady_fill_hours,last_row
    last_row = []
    del_line = filled_list.pop()
    print("删除了\n [-] ->"+str(del_line))
    r_alrady_fill_hours -= del_line[7]
    time.sleep(2)

def print_from() -> None:
    '''
    打印所有填写的行
    '''
    tmp = sort_by_day()
    os_clear()
    print("已填写表单")
    hr()
    for line in tmp:
        print(line)
    hr()
    time.sleep(4)

def cache_dump() -> None:
    '''
    写入缓存到磁盘
    '''
    global cacher,r_totalhours
    cacher["hour"] = r_totalhours
    with open("cache","w",encoding="utf-8") as f:
        writer = json.dump(cacher,f)

def cache_load() -> None:
    '''
    读取缓存中的数据
    '''
    global r_alrady_fill_hours,filled_list,r_totalhours
    with open("cache","r",encoding="utf-8") as f:
        cacher = json.load(f)
    r_totalhours = cacher["hour"]
    filled_list = cacher['c']
    for line in filled_list:
        if line:
            #calc hour
            r_alrady_fill_hours += float(line[7])
            #add reson
            if line[8] not in r_reasons:
                r_reasons.append(line[8])
    os_msg("successfully load data from cache")

def sort_by_day() -> None:
    '''
    根据日期排列已填写的行
    '''
    global filled_list
    return sorted(filled_list,key=lambda x: int(x[1]))

while True:
    os_clear()
    print_lastrow()
    always_display()

    templist = new_row()
    day = input(f"{r_month}月 您这是在编第几天的 > ")
    # 判断输入
    # TODO 更改判断方式
    if day =="wq":
        break
    elif day == "dl":
        remove_last()
        continue
    elif day == "p":
        print_from()
        continue
    elif day == "lc":
        cache_load()
        continue
    else:
        # 判断日期合法性
        if int(day) > (calendar.monthrange(r_year, r_month)[1]):
            os_msg("⚠Invaild input date")
            exit()
        templist[1] = day
    start_hour = int(input(f"{r_month}月{day}日 (hhmm[24小时制]整点要写00) 你啥时候开始工作的 > "))
    if start_hour > 1200:
        templist[2] = "下午"
    work_hour = float(input(f"{r_month}月{day}日 您干活干了多久(h) > "))
    templist[7] = work_hour
    r_alrady_fill_hours += work_hour
    calc_hour = datetime.datetime(2022,1,1,int(start_hour / 100),int(start_hour % 100))
    end_hour = calc_hour + datetime.timedelta(hours=work_hour)
    templist[6] = f"{calc_hour.strftime('%H:%M')} - {end_hour.strftime('%H:%M')}"
    works = str(input(f"{r_month}月{day}日 工作事项(选择事项id或者直接键入事项) > "))
    # 判断输入事项是否是事项id且事项id是否在已有的事项中
    if works.isdigit() and int(works) < len(r_reasons):
        templist[8] = r_reasons[int(works)]
    else:
        templist[8] = works
        r_reasons.append(works) 
    # 向已填写的数组添加新填写的行   
    filled_list.append(templist)
    last_row = templist
    # 在开启缓存时将现有的行加入缓存
    if r_cacheenable:
        cacher['c'].append(templist)
        cache_dump()

filled_list = sort_by_day()
print(f"填写结束保存至  [ 学生劳务工作登记表{r_month}月 {r_name}.xlsx ]  ")
app=xw.App(visible=True,add_book=False)
wb:xw.Book=app.books.open("template.xlsx")
sheet: xw.Sheet= wb.sheets[0]
if r_alrady_fill_hours > r_totalhours:
    sheet.range('H6').value=r_alrady_fill_hours
else:
    sheet.range('H6').value=r_totalhours

# 添加行
for i in range(len(filled_list)):
    sheet.api.Rows(5).Insert()
# 写入数据
for i in range(len(filled_list)):
    sheet.range(f'a{str(5+i)}').value = filled_list[i]

wb.save(f"学生劳务工作登记表{r_month}月 {r_name}.xlsx")

#关闭工作表并推出程式
wb.close()
app.quit()
